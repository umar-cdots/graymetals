Dear Sir

{{ $instruction->instructions }}

Datetime: {{ $instruction->pickup_datetime->format('l d/m/Y \a\t ha') }}
Loading from: {{ $instruction->shippingCompany->name }}
Going to: {{ $instruction->consignment->dischargePort->name }}
Doors to: {{ $instruction->door_orientation }}

Thanks